from django.shortcuts import render
from .models import AutomobileVO, Technician, Appointment
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

class AutomobileEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "sold",
        ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }



# Create your views here.
@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technicians = Technician.objects.create(**content)
            return JsonResponse(
                technicians,
                encoder=TechnicianEncoder,
                safe=False,
        )

        except:
            response = JsonResponse(
                {"message": "Could not create Technician"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def api_delete_tech(request, pk):
    request.method == "DELETE"
    count, _ = Technician.objects.filter(id=pk).delete()
    return JsonResponse({"deleted": count > 0})




@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            tech_id = content["technician"]
            technician = Technician.objects.get(id=tech_id)

            content["technician"] = technician
            
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
        )
        except:
            response = JsonResponse(
                {"message": "Could not create Appointment"}
            )
            response.status_code = 400
            return response



@require_http_methods(["GET", "POST", "PUT"])
def api_delete_appointment(request, pk, action=None):
    try:
        model = Appointment.objects.get(id=pk)
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Does not exist"}, status=404)

    if request.method == "GET":
        return JsonResponse(model, encoder=AppointmentEncoder, safe=False)
    elif request.method == "DELETE":
        model.delete()
        return JsonResponse({"message": "Appointment deleted successfully"})

    if action == "cancel":
        model.status = "canceled"
    elif action == "finish":
        model.status = "finished"
    else:
        return JsonResponse({"message": "Invalid action for PUT request"}, status=405)

    model.save()
    return JsonResponse(model, encoder=AppointmentEncoder, safe=False)
