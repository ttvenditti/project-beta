from django.urls import path

from .views import (
    api_technicians,
    api_delete_tech,
    api_appointments,
    api_delete_appointment,

)

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("technicians/create/", api_technicians, name="api_technicians"),
    path("technicians/<int:pk>/", api_delete_tech, name="api_delete_tech"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointments/create/", api_appointments, name="api_appointments"),
    path("appointments/<int:pk>/", api_delete_appointment, name="api_delete_appointment"),
    path("appointments/<int:pk>/cancel/", api_delete_appointment, {"action": "cancel"}, name="api_delete_appointment"),
    path("appointments/<int:pk>/finish/", api_delete_appointment, {"action": "finish"}, name="api_delete_appointment"),
]
