from django.db import models

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True, null=True)
    sold = models.BooleanField(default=False)



class Technician(models.Model):
    first_name = models.CharField(max_length=200, null =True)
    last_name = models.CharField(max_length=200, null =True)
    employee_id = models.PositiveSmallIntegerField(null=True, blank=True)



class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200, null=True, blank=True)
    status = models.CharField(max_length=200, null=True, blank=True)
    vin = models.CharField(max_length=200, null=True, blank=True)
    customer = models.CharField(max_length=200, null=True, blank=True)

    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.CASCADE,
    )
