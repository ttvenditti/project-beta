import React, {useState, useEffect} from 'react';

function SalesForm(){
    const [automobile, setAutomobile] = useState('');
    const [customer, setCustomer] = useState('');
    const [salesperson, setSalesperson] = useState('');
    const [automobiles, setAutomobiles] = useState([]);
    const [salespersons, setSalespersons] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [price, setPrice] = useState('');



    const fetchData = async()=>{
      const automobilesResponse = await fetch('http://localhost:8100/api/automobiles/');
      if (automobilesResponse.ok){
        const automobilesData = await automobilesResponse.json();
        // const unsoldAutos = automobilesData.autos.filter(auto => !auto.sold);
        setAutomobiles(automobilesData.autos);
      }

      const salespersonsResponse = await fetch('http://localhost:8090/api/salespeople/');
      if (salespersonsResponse.ok){
        const salespersonsData = await salespersonsResponse.json();
        setSalespersons(salespersonsData.salespersons);
      }

      const customersResponse = await fetch('http://localhost:8090/api/customers/');
      if (customersResponse.ok){
        const customersData = await customersResponse.json();
        setCustomers(customersData.customers);
      }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.price = price;
        data.salesperson = salesperson;
        data.customer = customer;
        data.automobile = automobile;


        const url = `http://localhost:8090/api/sales/create/`;

        const fetchConfig = {
            method:'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type':'application/json'
            },
        };


        const response = await fetch(url, fetchConfig);

        const autourl = `http://localhost:8100/api/automobiles/${automobile}/`;
        const autoData = {sold: true};
        const fetchConfigg = {
            method:'PUT',
            body: JSON.stringify(autoData),
            headers: {
                'Content-Type':'application/json'
            },

        }

        const autoresponse = await fetch(autourl, fetchConfigg);


        if (response.ok) {
          setCustomer('');
          setAutomobile('')
          setSalesperson('')
          setPrice('')
          fetchData()
        }

        if (autoresponse.ok){

        }
    }

    const handleCustomerChange = (event) => {
      const value = event.target.value;
      setCustomer(value);
    }

    const handleAutomobileChange = (event) => {
      const value = event.target.value;
      setAutomobile(value);
    }

    const handleSalespersonChange = (event) => {
      const value = event.target.value;
      setSalesperson(value);
    }

    const handlePriceChange = (event) => {
      const value = event.target.value;
      setPrice(value);
    }


    return(
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Record a new Sale</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form">

            <div className="form-floating mb-3">
              <select onChange={handleAutomobileChange} value={automobile} required name="automobile" id="automobile" className="form-select">
                <option value="">Choose an Automobile Vin...</option>
                  {automobiles
                  .filter(automobile => automobile.sold === false)
                  .map(automobile => {
                    return (
                      <option key={automobile.id} value={automobile.vin}>{automobile.vin}</option>
                    )
                  })}
              </select>
            </div>

            <div className="form-floating mb-3">
              <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                <option value="">Choose a Salesperson...</option>
                  {salespersons.map(salesperson => {
                    return (
                      <option key={salesperson.id} value={salesperson.id}>{`${salesperson.first_name} ${salesperson.last_name}`}</option>
                    )
                  })}
              </select>
            </div>

            <div className="form-floating mb-3">
              <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
                <option value="">Choose a Customer...</option>
                  {customers.map(customer => {
                    return (
                      <option key={customer.id} value={customer.id}>{`${customer.first_name} ${customer.last_name}`}</option>
                    )
                  })}
              </select>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handlePriceChange} value={price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
              <label htmlFor="price">Price...</label>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    )
}
export default SalesForm
