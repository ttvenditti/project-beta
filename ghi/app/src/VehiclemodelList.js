import {React, useState, useEffect} from 'react';

function VehiclemodelList(){
    const[vehiclemodels, setVehiclemodels] = useState([]);

    const getData = async()=>{
        const response = await fetch('http://localhost:8100/api/models/');
        if(response.ok){
            const data = await response.json();
            setVehiclemodels(data.models);
        } else{
            console.error("An error occured fetching the data")
        }
    }

    useEffect(()=>{
        getData()
      }, []);

    return(
        <div className='my-5 container'>
          <div className='row'>
            <h1>Models</h1>
            <table className='table table-striped m-3'>
              <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
              </thead>
              <tbody>
                {vehiclemodels.map(vehiclemodel =>{
                    return (
                      <tr key={vehiclemodel.id}>
                        <td>{vehiclemodel.name}</td>
                        <td>{vehiclemodel.manufacturer.name}</td>
                        <td><img src={ vehiclemodel.picture_url } alt="Picture of Model" height="100" width="200"/></td>
                      </tr>
                    );
                })}
              </tbody>
            </table>
          </div>
        </div>
    );
}
export default VehiclemodelList;
