import {useState, useEffect} from "react";

function SalespersonHistory(){
    const [salespersons, setSalespersons] = useState([])
    const [persons, setPersons] = useState([])
    const [fitlerValue, setFilterValue] = useState("")


    async function getData(){
        const response = await fetch("http://localhost:8090/api/sales/")
        if (response.ok){
            const data = await response.json();
            setSalespersons(data.sales.map((sale)=>{
                return {
                    vin: sale.automobile.vin,
                    salespersonName: `${sale.salesperson.first_name} ${sale.salesperson.last_name}`,
                    customerName: `${sale.customer.first_name} ${sale.customer.last_name}`,
                    price: sale.price
                }
            }));
        }
        const personResponse = await fetch("http://localhost:8090/api/salespeople/")
        if (response.ok){
          const data = await personResponse.json()
          setPersons(data.salespersons.map((salesperson)=>{
            return {
              salespersonName:`${salesperson.first_name} ${salesperson.last_name}`,
            }
          }))
        }
    }

    useEffect(() => {
        getData();
    }, []);

    function handleFilterChange(e){
        setFilterValue(e.target.value);
    }



    return(
        <div className='my-5 container'>
          <div className='row'>
            <h1>Salesperson History</h1>
            {/* <input onChange={handleFilterChange} placeholder="search by salesperson name..." /> */}
            <select value={fitlerValue} onChange={handleFilterChange}>
                    <option value="">Select a salesperson</option>
                    {persons.map((salesperson, index) => (
                        <option key={index} value={salesperson.salespersonName}>
                            {salesperson.salespersonName}
                        </option>
                    ))}
                </select>
            <table className='table table-striped m-3'>
              <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
              </thead>
              <tbody>
                {salespersons
                  // .filter((salesperson) => salesperson.salespersonName.includes(fitlerValue))
                  .filter((sale) =>
                              fitlerValue
                                ? sale.salespersonName === fitlerValue
                                : true
                          )
                  .map((salesperson, index) => (
                    <tr key={index}>
                      <td>{salesperson.salespersonName}</td>

                      <td>{salesperson.customerName}</td>

                      <td>{salesperson.vin}</td>
                      <td>{salesperson.price}</td>
                    </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
    )
}
export default SalespersonHistory
