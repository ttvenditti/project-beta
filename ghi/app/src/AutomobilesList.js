import { useState, useEffect } from "react";

function AutomobilesList() {
  const [autos, setAutos] = useState([]);



  const fetchData = async () => {
    try {
      const automobilesResponse = await fetch(
        "http://localhost:8100/api/automobiles/"
      );

      if (automobilesResponse.ok) {
        const data = await automobilesResponse.json();

        setAutos(data.autos);
      } else {
        console.error("An error occurred fetching data");
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };


  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="my-5 container">
      <div className="row">
        <h1>Automobile List</h1>

        <table className="table table-striped m-3">
          <thead>
            <tr>
              <th>Color</th>
              <th>Year</th>
              <th>Vin</th>
              <th>Model name</th>
              <th>Manufacturer</th>
              <th>Sold</th>
            </tr>
          </thead>
          <tbody>
            {autos.map((auto) => (
              <tr key={auto.id}>
                <td>{auto.color}</td>
                <td>{auto.year}</td>
                <td>{auto.vin}</td>
                <td>{auto.model.name}</td>
                <td>{auto.model.manufacturer.name}</td>
                <td>{auto.sold ? "Yes" : "No"}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default AutomobilesList;
