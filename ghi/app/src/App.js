import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AppointmentList from "./AppointmentList";
import AppointmentForm from "./AppointmentForm";
import TechnicianList from "./TechnicianList";
import TechnicianForm from "./TechnicianForm";
import ServiceHistory from "./ServiceHistory";
import SalespersonList from './SalespersonList';
import SalespersonForm from './SalespersonForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import SalespersonHistory from './SalespersonsHistory';
import ManufacturerList from './ManufacturersList';
import ManufacturerForm from './ManufacturersForm';
import VehiclemodelList from './VehiclemodelList';
import ModelForm from "./ModelForm";
import AutomobilesList from "./AutomobilesList";
import AutomobilesForm from "./AutomobilesForm";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/automobiles/create" element={<AutomobilesForm />} />
          <Route path="/automobiles" element={<AutomobilesList />} />
          <Route path="/models/create" element={<ModelForm />} />

          <Route path="/" element={<MainPage />} />

          <Route path="/manufacturers" element={<ManufacturerList/>}/>
          <Route path="/manufacturers/create" element={<ManufacturerForm/>}/>

          <Route path="/models" element={<VehiclemodelList/>}/>
          <Route path="/appointments/history" element={<ServiceHistory />} />

          <Route path="/appointments" element={<AppointmentList />} />
          <Route path="/appointments/create" element={<AppointmentForm />} />

          <Route path="/technicians" element={<TechnicianList />} />
          <Route path="/technicians/create" element={<TechnicianForm />} />

          <Route path="salespeople" element={<SalespersonList />} />
          <Route path="salespeople/new" element={<SalespersonForm />} />

          <Route path="customers" element={<CustomerList />} />
          <Route path="customers/new" element={<CustomerForm />} />

          <Route path="sales" element={<SalesList />} />
          <Route path="sales/new" element={<SalesForm />} />
          <Route path="sales/history" element={<SalespersonHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
