import { useState, useEffect } from "react";

function TechnicianList() {
  const [technicians, setTechnicians] = useState([]); 

  const getData = async () => {
    try {
      const response = await fetch("http://localhost:8080/api/technicians/");
      if (response.ok) {
        const { technicians } = await response.json();
        setTechnicians(technicians);
      } else {
        console.error("An error occurred fetching the data");
      }
    } catch (error) {
      console.error("An unexpected error occurred:", error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className="my-5 container">
      <div className="row">
        <h1>Current Technicians</h1>

        <table className="table table-striped m-3">
          <thead>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Employee ID</th>
            </tr>
          </thead>
          <tbody>
            {technicians.map((technician) => {
              return (
                <tr key={technician.id}>
                  <td>{technician.first_name}</td>
                  <td>{technician.last_name}</td>
                  <td>{technician.employee_id}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default TechnicianList;
