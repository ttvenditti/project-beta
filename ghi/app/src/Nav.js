import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">
                Home
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="manufacturers"
              >
                Manufacturers
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="manufacturers/create"
              >
                Add a Manufacturer
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="models"
              >
                Models
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="automobiles/create"
              >
                Create an Automobile
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="automobiles"
              >
                Automobiles List
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="models/create"
              >
                Create a model
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="appointments/history"
              >
                Service History
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="technicians"
              >
                Technicians
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="technicians/create"
              >
                Add a Technician
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="appointments"
              >
                Service appointments
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="appointments/create"
              >
                Create a service appointment
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link active" to="/salespeople">
                List Salespeople
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link active" to="/salespeople/new">
                Add a Salespeople
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link active" to="/customers">
                List Customers
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link active" to="/customers/new">
                Add a Customer
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link active" to="/sales">
                Sales
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link active" to="/sales/new">
                Add a Sale
              </NavLink>
            </li>

          <li className="nav-item">
            <NavLink className="nav-link active" to="/sales/history">Salesperson History</NavLink>
          </li>

            <li className="nav-item">
              <NavLink className="nav-link" to="/sales/history">
                Salesperson History
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
