
import { useState, useEffect } from "react";

function ServiceHistory() {
  const [appointment, setAppointments] = useState([]);
    const [autos, setAutos] = useState([]);
    const [reset, setReset] = useState([]);
    const [filters, setFilter] = useState([]);

  const fetchData = async () => {
    try {
      const appointmentsResponse = await fetch(
        "http://localhost:8080/api/appointments/"
      );
      const autosResponse = await fetch(
        "http://localhost:8100/api/automobiles/"
      );

      if (appointmentsResponse.ok && autosResponse.ok) {
        const { appointment } = await appointmentsResponse.json();
        const { autos } = await autosResponse.json();

        setAppointments(appointment);
        setAutos(autos);
        setFilter(appointment)
      } else {
        console.error("An error occurred fetching data");
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };



  useEffect(() => {
    fetchData();
  }, []);

    const handleSearch = () => {
      const filtered = appointment.filter(app =>
        app.vin.toLowerCase().includes(reset.toLowerCase())
      );
      setFilter(filtered);
      
    };
     useEffect(() => {
       if (reset === "") {
         setFilter(appointment);
       }
     }, [reset, appointment]);
  return (
    <div className="my-5 container">
      <div className="row">
        <h1>Service Appointments</h1>
          <div className="input-group mb-3">
            <input
              type="text"
                placeholder="Search by VIN"
                  value={reset}
                    onChange={(e) => setReset(e.target.value)}
                      className="form-control"
                        />
                          <div className="input-group-append">
                          <button
                        className="btn btn-primary"
                      type="button"
                    onClick={handleSearch}
                  >
                Search
               </button>
            </div>
          </div>
        </div>
        <table className="table table-striped m-3">
          <thead>
            <tr>
              <th>Vin</th>
              <th>VIP</th>
              <th>Customer</th>
              <th>Date/Time</th>
              <th>Technician</th>
              <th>Reason</th>
            </tr>
          </thead>
          <tbody>
            {filters.map((app) => (
              <tr key={app.id}>
                <td>{app.vin}</td>
                <td>
                  {autos.some((auto) => auto.vin === app.vin) ? "Yes" : "No"}
                </td>
                <td>{app.customer}</td>
                <td>{new Date(app.date_time).toLocaleString()}</td>
                <td>{app.technician.first_name}</td>
                <td>{app.reason}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

  );
}

export default ServiceHistory;
