# CarCar
CarCar is an application made for the needs of automobile sales, and maintenance stores. CarCar allows its users to manage vehicle inventory, sales, service, maintenance, personnel, and business history.

Team:

* Trevor Venditti - Service
* Justin Ryu - Sales

## Set-up Instructions

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository
<<https://gitlab.com/ttvenditti/project-beta>>

2. Clone the forked repository onto your local computer by following these steps:

Open your work folder in the terminal and paste the clone copy in the terminal with this command:

git clone <<respository.url.here>>

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/


## Design

CarCar is made up of 3 microservices that interact with one another.
- **Inventory**
- **Service**
- **Sales**

![alt text](image.png)

https://excalidraw.com/#json=4NEsZ_bpCtfiRzG7cy4Vv,ldVAybKk1EEf5qgtUvZJKw

## CRUD Tables:

## Automobiles

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Automobiles | GET | http://localhost:8100/api/automobiles/
| Create an Automobile | POST | http://localhost:8100/api/automobiles/
| Delete an Automobile | Delete | http://localhost:8100/api/automobiles/:vin/


## Vehicle Models

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Vehicle Models | GET | http://localhost:8100/api/models/
| Details of Vehicle Model | GET | http://localhost:8100/api/models/:id/
| Create a Vehicle Model | POST | http://localhost:8100/api/models/
| Delete a Vehicle Model | Delete | http://localhost:8100/api/models/:id/


## Manufacturers

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Manufacturers | GET | http://localhost:8100/api/manufacturers/
| Details of Manufacturers | GET | http://localhost:8100/api/manufacturers/:id/
| Create a Manufacturer | POST | http://localhost:8100/api/manufacturers/
| Delete a Manufacturer | Delete | http://localhost:8100/api/manufacturers/:id/


## Technicians

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Technicians | GET | http://localhost:8080/api/technicians/
| Create a Technician | POST | http://localhost:8080/api/technicians/
| Delete a Technician | Delete | http://localhost:8080/api/technicians/:id/


## Appointments

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Salespeople | GET | http://localhost:8080/api/appointments/
| Create an Appointment | POST | http://localhost:8080/api/appointments/
| Delete an Appointment | Delete | http://localhost:8080/api/appointments/:id/
| Cancel an Appointment | PUT |http://localhost:8080/api/appointments/:id/cancel/
| Finish an Appointment | PUT |http://localhost:8080/api/appointments/:id/finish/


## Salespeople

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Salespeople | GET | http://localhost:8090/api/salespeople/
| Create an Salesperson | POST | http://localhost:8090/api/salespeople/
| Delete an Salesperson | Delete | http://localhost:8090/api/salespeople/:id/


## Customers

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Customers | GET | http://localhost:8090/api/customers/
| Create an Customer | POST | http://localhost:8090/api/customers/
| Delete an Customer | Delete | http://localhost:8090/api/customers/:id/


## Sales

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Sales | GET | http://localhost:8090/api/sales/
| Create an Automobile | POST | http://localhost:8090/api/sales/
| Delete an Automobile | Delete | http://localhost:8090/api/sales/:id


## Service microservice

Within the Service microservice there are three models (Appointments, Technicians, AutomobileVO(Value Object)) These models provide the names and data types of the instances we present to the front end. It integrates with the Inventory microservice through the AutomobileVO to pull data from the Automobile model, which includes data from the Manufacturer, and VehicleModel models. We use a poller that constantly checks the Inventory microservice for changes and updates the Service microservice with the new information. This results in a  display for the user to assign a specific Technician(Services) to a specific Automobile(Inventory) so they can be assigned an appointment date and time for service.


## Sales microservice

The sales microservice contains four different models: Salesperson, Customer, Sale, and AutomobileVO(Value Object). The Sale model interacts with the other three and uses the data within them. The Sales microservice interacts with the Inventory microservice through the AutomobleVO that pulls data about automobiles from inventory. There is a sales poller that constantly checks the inventory microservice every 60 seconds for updates or changes. The reason for the integration is when we create a new sale, an existing automobile needs to be chose. Therefore the sale microservice needs the automobile data from the inventory microservice in order to fill out a sale.

Explain your models and integration with the inventory
microservice, here.
