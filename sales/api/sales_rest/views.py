from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Salesperson, Customer, AutomobileVO, Sale

# Create your views here.

class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "customer",
        "automobile",
        "salesperson"
    ]
    encoders = {
        "customer":CustomerEncoder(),
        "automobile":AutomobileVOEncoder(),
        "salesperson":SalespersonListEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_salesperson(request):

    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalespersonListEncoder,
        )

    else:
        try:
            content = json.loads(request.body)
            salespersons = Salesperson.objects.create(**content)
            return JsonResponse(
                salespersons,
                encoder=SalespersonListEncoder,
                safe=False,
        )
        except:
            response = JsonResponse(
                {"message": "Could not create a Salesperson"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def api_delete_salesperson(request, pk):
    request.method == "DELETE"
    count, _ = Salesperson.objects.filter(id=pk).delete()
    return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers":customers},
            encoder = CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customers = Customer.objects.create(**content)
            return JsonResponse(
            customers,
            encoder = CustomerEncoder,
            safe=False
            )
        except:
            response = JsonResponse(
                {"message":"Could not create customer"},
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def api_delete_customer(request, pk):
    request.method == "DELETE"
    count, _ = Customer.objects.filter(id=pk).delete()
    return JsonResponse(
        {"deleted" : count > 0}
    )

@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            customer_id=content["customer"]
            salesperson_id=content["salesperson"]
            automobile_id = content["automobile"]

            automobile = AutomobileVO.objects.get(vin = automobile_id)
            customer = Customer.objects.get(id=customer_id)
            salesperson = Salesperson.objects.get(id=salesperson_id)

            content["customer"] = customer
            content["salesperson"] = salesperson
            content["automobile"] = automobile


            sales = Sale.objects.create(**content)

            return JsonResponse(
                sales,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse(
                {"message": "Could not create Sale"}
            )
            response.status_code = 400
            return response
