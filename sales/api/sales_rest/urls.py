from django.urls import path
from .views import api_salesperson, api_delete_salesperson, api_customer, api_delete_customer, api_sales

urlpatterns = [
    path('salespeople/', api_salesperson, name='api_salesperson'),
    path('salespeople/<int:pk>/', api_delete_salesperson, name='api_delete_salesperson'),
    path ('salespeople/create/', api_salesperson, name="api_create_salesperson"),

    path('customers/', api_customer, name="api_customer"),
    path('customers/<int:pk>/', api_delete_customer, name="api_delete_customer"),
    path ('customers/create/', api_customer, name="api_create_customer"),

    path('sales/', api_sales, name='api_sales'),
    path('sales/create/', api_sales, name='api_create_sales'),

]
